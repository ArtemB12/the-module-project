import { React, useMemo } from 'react';
import MoviesList from '../components/Movies/MoviesList';
import NavBar from '../components/NavBar/NavBar';
import { useSelector } from "react-redux";


export default function Favourites() {
    const { likedMovies } = useSelector((state) => state.movies);
    console.log(likedMovies);
    
    // const likedMovies = movies.filter(m => m.isLiked === true);

    /* const likedMovies = useMemo(() => {
        return [...movies].filter((m) => m.isLiked);
      }, [movies]); */
    

    return(
        <>
            <NavBar/>
            <MoviesList movies={likedMovies}/>
        </>
    )
}