import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// import NavBar from '../../components/UI/NavBar/NavBar';
// import Card from "../../components/Card/Card";
import MovieCard from "../components/Movie/MovieCard";
import { useParams, useNavigate } from 'react-router-dom';
import { fetchMovieById } from "../store/reducers/moviesSlice";
import NavBar from "../components/NavBar/NavBar";

function MovieInfo() {
    const { movie } = useSelector((state) => state.movies);
  const { id } = useParams();
  console.log(id)
  
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovieById(id));
  }, [dispatch, id]);

  return (
    <div className="tv-show-wrapper">
        <NavBar />
        <MovieCard movie={movie} />
    </div>
  );
}

export default MovieInfo;
