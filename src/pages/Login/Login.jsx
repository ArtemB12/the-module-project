import React from "react";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import "./Login.scss";
import LoginForm from "../../components/LoginForm/LoginForm";
import { toggleModal } from "../../store/reducers/signUpSlice";
import SignInDialog from "../../components/SignInDialog/SignInDialog";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Login() {
  const { isOpenModal, signUpSuccess } = useSelector((state) => state.signUp);

  const dispatch = useDispatch();

  const handleToggleModal = () => {
    dispatch(toggleModal());
    if (signUpSuccess && isOpenModal) {
      notify();
    }
  };

  const notify = () =>
    toast.success("Your account has been created successfully!", {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  return (
    <div className="login-wrapper">
      <LoginForm />
      <Button
        onClick={handleToggleModal}
        variant="outlined"
        className="signin-btn"
      >
        Sign up
      </Button>
      <SignInDialog open={isOpenModal} onClose={handleToggleModal} />
      <ToastContainer
        position="bottom-left"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default Login;
