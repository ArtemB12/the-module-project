import { React, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MoviesList from "../components/Movies/MoviesList";
import NavBar from "../components/NavBar/NavBar";
import Pagination from "../components/UI/Pagination";
import { fetchMovies } from "../store/reducers/moviesSlice";
import { changePage } from "../store/reducers/moviesSlice";

export default function Dashboard() {
  const { movies, totalPages, page } = useSelector((state) => state.movies);
  // const [page, setPage] = useState(1);
  console.log(movies);
  
  const dispatch = useDispatch();

  /* useEffect(() => {
    dispatch(fetchMovies({ page }));
  }, [dispatch, page]); */

  function setNewPage(_, value) {
    // setPage(value);
    dispatch(changePage(value));
  }

 /*  const updatedMovies = useMemo(() => {
      return [...movies].map((m) => ({...m, isLiked: false}));
  }, [movies]); */

  /* useMemo(() => {
    dispatch(addMovies(updatedMovies));
  }, [dispatch]); */

  // console.log(updatedMovies);

  return (
    <>
      <NavBar/>
      <Pagination count={totalPages} page={page} setNewPage={setNewPage}/>
      <MoviesList movies={movies} />
      <Pagination count={totalPages} page={page} setNewPage={setNewPage}/>
    </>
  );
}
