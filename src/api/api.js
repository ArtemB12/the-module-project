import axios from 'axios';
import { ACCESS_KEY } from '../config/config';

const movieAPI = axios.create({
    baseURL: 'https://api.themoviedb.org/3',
    params: {
      api_key: ACCESS_KEY,
      language: 'en-US',
    }
});

export const searchPopularMovies = ({ page: page }) => {
    return movieAPI.get('/movie/popular', {
        params: {
            page,
        }
    });
}

export const searchMovieById = (id) => {
    return movieAPI.get(`/movie/${id}`);
}

export const searchMovieByName = (name) => {
    return movieAPI.get(`/search/movie`, {
        params: {
            query: name,
        }
    });
}