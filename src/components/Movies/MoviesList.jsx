import "./MoviesList.scss";
import { useNavigate } from "react-router-dom";
import LikeButton from "../LikeButton/LikeButton";
import Typography from "@mui/material/Typography";

function MoviesList({ movies }) {
  const navigate = useNavigate();

  function navigateToMovieInfo(cardId) {
    navigate(`/movie-info/${cardId}`, { replace: true });
  }

  return (
    <ul className="card-list">
      {movies.map((card) => (
        <li
          className="card"
          key={card.id}
        >
          <a className="card-image" onClick={() => navigateToMovieInfo(card.id)}>
            <img
              src={"https://image.tmdb.org/t/p/w500" + card.poster_path}
              alt={card.name}
            />
          </a>
          <a className="card-description">
            <Typography gutterBottom variant="subtitle1" component="div">
              {card.title}
            </Typography>
            <LikeButton movie={card} />
          </a>
        </li>
      ))}
    </ul>
  );
}

export default MoviesList;
