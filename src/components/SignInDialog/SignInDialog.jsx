import { React, useState } from "react";
import PropTypes from "prop-types";
import Dialog from "@mui/material/Dialog";
import Box from "@mui/material/Box";
import * as yup from "yup";
import "./SignInDialog.scss";
import { useForm } from "react-hook-form";
import { setUserInfo, toggleModal } from "../../store/reducers/signUpSlice";
import { useDispatch, useSelector } from "react-redux";
import { yupResolver } from "@hookform/resolvers/yup";
import Input from "../UI/Input";
import Button from "@mui/material/Button";
// import DateInput from '../UI/DateInput';
import dayjs from "dayjs";
import TextField from "@mui/material/TextField";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

const schema = yup
  .object({
    firstName: yup
      .string()
      .matches(/^[A-Za-z]+$/i, "Please, enter correct name")
      .required(),
    lastName: yup
      .string()
      .matches(/^[A-Za-z]+$/i, "Please, enter correct name")
      .required(),
    username: yup
      .string()
      .matches(/^[a-z][a-zA-Z\d\.\_]+$/i, "Please, enter correct username")
      .required(),
    email: yup.string().email().required(),
    password: yup.string().required("This field is required"),
    confirmedPassword: yup.string().when("password", {
      is: val => (val && val.length > 0 ? true : false),
      then: yup.string().oneOf(
        [yup.ref("password")],
        'Both password need to be the same'
      )
    })
  })
  .required();

export default function SignInDialog({ onClose, open }) {
  const { name, surname, email, username, password, confirmedPassword } = useSelector(
    (state) => state.signUp
  );

  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    console.log(data);
    dispatch(setUserInfo(data));
    dispatch(toggleModal());
    notify();
  };

  const [value, setValue] = useState(dayjs(new Date()));

  const handleDateChange = (newValue) => {
    setValue(newValue);
  };

  const notify = () =>
    toast.success("Your account has been created successfully!", {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  const [gender, setGender] = useState("");

  const handleGenderChange = (event) => {
    setGender(event.target.value);
  };

  return (
    <Dialog onClose={onClose} open={open}>
      <Box className="form-wrapper">
        <h3 className="form-title">Register Form</h3>
        <form onSubmit={handleSubmit(onSubmit)} className="form-table">
          <Input
            defaultValue={name}
            placeholder="First Name"
            {...register("firstName")}
          />
          <p className="form-error">{errors.firstName?.message}</p>
          <Input
            defaultValue={surname}
            placeholder="Last Name"
            {...register("lastName")}
          />
          <p className="form-error">{errors.lastName?.message}</p>
          <Input
            defaultValue={username}
            placeholder="Username"
            {...register("username")}
          />
          <p className="form-error">{errors.username?.message}</p>
          <Input
            defaultValue={email}
            placeholder="Email"
            {...register("email")}
          />
          <p className="form-error">{errors.email?.message}</p>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DesktopDatePicker
              label="Birth date"
              inputFormat="DD/MM/YYYY"
              value={value}
              onChange={handleDateChange}
              {...register("date")}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
          <p className="form-error">{errors.date?.message}</p>
          <FormControl fullWidth>
            <InputLabel id="simple-select-label">Gender</InputLabel>
            <Select
              {...register("gender")}
              labelId="simple-select-label"
              id="simple-select"
              value={gender}
              label="Gender"
              onChange={handleGenderChange}
            >
              <MenuItem value={"Male"}>Male</MenuItem>
              <MenuItem value={"Female"}>Female</MenuItem>
            </Select>
          </FormControl>
          <p className="form-error">{errors.gender?.message}</p>
          <Input
                defaultValue={password}         
                placeholder="Password" 
                {...register("password")}
            /> 
            <p className="form-error">{errors.password?.message}</p>
            <Input     
                defaultValue={confirmedPassword} 
                placeholder="Confirm password" 
                {...register("confirmedPassword")}
            /> 
            <p className="form-error">{errors.confirmedPassword?.message}</p>
          <ToastContainer
            position="bottom-left"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
          <Button type="submit" variant="contained">
            Submit
          </Button>
        </form>
      </Box>
    </Dialog>
  );
}

SignInDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  // selectedValue: PropTypes.string.isRequired
};
