import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from "@mui/material/Button";
import { useNavigate } from 'react-router-dom';
import './NavBar.scss';
import SearchInput from "../UI/SearchInput";

export default function NavBar() {
    const navigate = useNavigate();

  function navigateToMovies() {
    navigate('/');
  }

  function navigateToFavorites() {
    navigate('/favorites');
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <ButtonGroup variant="text" aria-label="text button group">
            <Button className="btn" onClick={navigateToMovies}>Movies</Button>
            <Button className="btn" onClick={navigateToFavorites}>Favorites</Button>
          </ButtonGroup>
          <SearchInput/>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
