import { React, useState } from "react";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from "@mui/icons-material/Search";
import TextField from "@mui/material/TextField";
import { useDispatch } from "react-redux";
import { fetchMovieByName } from "../../store/reducers/moviesSlice";

export default function SearchInput() {
  const [movie, setMovie] = useState("");
  const dispatch = useDispatch();

  function handleSearchInput(e) {
    // let res = dispatch(searchMovieByName(e.target.value))
    console.log(e.target.value);
    setMovie(e.target.value);
  }

  async function onSubmit(e) {
    e.preventDefault();
    let res = dispatch(fetchMovieByName(movie));
    console.log(res);
  }

  return (
    <div style={{ display: "flex", marginLeft: "auto" }}>
      <form noValidate autoComplete="off" onSubmit={onSubmit}>
        <TextField
          onChange={handleSearchInput}
          id="standard-size-small"
          defaultValue=""
          size="small"
          variant="standard"
          label="Search for a movie..."
          sx={{ width: "300px", backgroundColor: "white" }}
          InputProps={{
            endAdornment: (
              <InputAdornment>
                <IconButton type="submit">
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </form>
    </div>
  );
}
