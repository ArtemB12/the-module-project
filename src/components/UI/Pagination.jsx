import * as React from "react";
import Typography from "@mui/material/Typography";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";

export default function PaginationCustom({ count, page, setNewPage }) {
  return (
    
      <Stack spacing={2} sx={{ marginTop: "10px", marginBottom: "10px", alignItems: "center" }}>
        <Pagination count={count} page={page} onChange={setNewPage} />
      </Stack>
    
  );
}
