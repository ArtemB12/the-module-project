import React, { forwardRef } from 'react';
import TextField from '@mui/material/TextField';

const Input = forwardRef((props, ref) => (
  <TextField 
      defaultValue={props.defaultValue}
      id="outlined-basic" 
      label={props.placeholder} 
      variant="outlined" 
      {...props}
      inputRef={ref}
      className="input"
  />
))

export default Input;
