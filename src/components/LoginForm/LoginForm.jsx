import { React } from 'react';
import Button from '@mui/material/Button';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import Input from '../UI/Input';
import { useNavigate } from 'react-router-dom';
import { setAuthToken } from '../../store/reducers/signUpSlice';
import { useDispatch, useSelector } from "react-redux";
import './LoginForm.scss'

const schema = yup.object().shape({
  username: yup.string().required('Username is required'),
  password: yup.string(),
});

function LoginForm() {
  const { authToken } = useSelector((state) => state.signUp);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const onSubmit = (event) => {
    console.log(event);
    localStorage.setItem('AUTH_TOKEN', 'qwerty');
    const authToken = localStorage.getItem('AUTH_TOKEN');
    dispatch(setAuthToken(authToken));
    navigate('/');
  };

  return (
    <form
      noValidate
      className='login-form'
      onSubmit={handleSubmit(onSubmit)}
    >
      <Input
        {...register('username')}
        id="username"
        type="text"
        label="Username"
        name="username"
        error={!!errors.username}
        helperText={errors?.username?.message}
      />
      <Input
        {...register('password')}
        id="password"
        type="password"
        label="Password"
        name="password"
        error={!!errors.password}
        helperText={errors?.password?.message}
      />
      <Button type="submit" fullWidth variant="contained" color="primary">
        Log in
      </Button>
    </form>
  );
}

export default LoginForm;
