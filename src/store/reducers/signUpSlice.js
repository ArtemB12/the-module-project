import { createSlice } from '@reduxjs/toolkit'

export const signUpSlice = createSlice({
    name: 'signUp',
    initialState: {
      isOpenModal: false,
      name: '',
      surname: '',
      username: '',
      email: '',
      authToken: '',
      birthDate: null,
      gender: '',
      password: '',
      confirmedPassword: '',
      signUpSuccess: false,
    },
    reducers: {
      toggleModal: (state) => {
        state.isOpenModal = !state.isOpenModal;
      },
      setUserInfo (state, action) {
        console.log(action);
        state.name = action.payload.firstName;
        state.surname = action.payload.lastName;
        state.username = action.payload.username;
        state.email = action.payload.email;
        state.birthDate = action.payload.date;
        state.gender = action.payload.gender;
        state.password = action.payload.password;
        state.confirmedPassword = action.payload.confirmedPassword;
        state.signUpSuccess = !state.signUpSuccess;
      },
      setAuthToken (state, action) {
        console.log(action);
        state.authToken = action.payload;
      }
    },
  })
  
export const { toggleModal, setUserInfo, setAuthToken } = signUpSlice.actions;
export default signUpSlice.reducer
