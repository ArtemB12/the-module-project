import { combineReducers } from 'redux';
import moviesReducer from './moviesSlice';
import signUpReducer from './signUpSlice';

export default combineReducers({
  movies: moviesReducer,
  signUp: signUpReducer,
});
