import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  searchMovieById,
  searchMovieByName,
  searchPopularMovies,
} from "../../api/api";

export const fetchMovies = createAsyncThunk(
  "movies/fetchMovies",
  async (params) => {
    const res = await searchPopularMovies(params);
    return res.data;
  }
);

export const fetchMovieById = createAsyncThunk(
  "movies/fetchMovieById",
  async (id) => {
    const res = await searchMovieById(id);
    console.log(res);
    return res.data;
  }
);

export const fetchMovieByName = createAsyncThunk(
  "movies/fetchMovieByName",
  async (name) => {
    const res = await searchMovieByName(name);
    console.log(res);
    return res.data;
  }
);

export const moviesSlice = createSlice({
  name: "movies",
  initialState: {
    movies: [],
    likedMovies: [],
    movie: {},
    page: 1,
    loaded: false,
    loading: false,
    errors: null,
    loadedMovie: false,
    loadingMovie: false,
    errorsMovie: null,
    totalPages: 0,
  },
  reducers: {
    addMovies: (state, action) => {
      console.log(action);
      state.movies = action.payload.movies;
    },
    handleLikeButton: (state, action) => {
      state.movies = state.movies.map((m) => {
        if (m.id === action.payload.id) {
          return { ...m, isLiked: !m.isLiked };
        }
        return m;
      });
      // state.likedMovies = [...state.movies].filter((m) => m.isLiked === true);
      // let filtered = [...state.movies].filter((m) => m.isLiked === true);
      // state.likedMovies = [...state.likedMovies, filtered]
      if (action.payload.isLiked) {
        state.likedMovies = [...state.likedMovies, action.payload]
      }
    },
    changePage: (state, action) => {
      console.log(action);
      state.page = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchMovies.pending, (state, action) => {
        state.loading = true;
        state.loaded = false;
        state.errors = null;
      })
      .addCase(fetchMovies.fulfilled, (state, action) => {
        console.log(action);
        state.movies = action.payload.results.map((m) => ({
          ...m,
          isLiked: false,
        }));
        state.totalPages = action.payload.total_pages;
        state.loaded = true;
        state.loading = false;
      })
      .addCase(fetchMovies.rejected, (state, action) => {
        state.errors = action.error;
        state.loaded = true;
        state.loading = false;
      })
      .addCase(fetchMovieById.pending, (state, action) => {
        state.loadingMovie = true;
        state.loadedMovie = false;
        state.errorsMovie = null;
      })
      .addCase(fetchMovieById.fulfilled, (state, action) => {
        console.log(action);
        state.movie = action.payload;
        state.loadedMovie = true;
        state.loadingMovie = false;
      })
      .addCase(fetchMovieById.rejected, (state, action) => {
        state.errorsMovie = action.error;
        state.loadedMovie = true;
        state.loadingMovie = false;
      })
      .addCase(fetchMovieByName.pending, (state, action) => {
        state.loadingMovie = true;
        state.loadedMovie = false;
        state.errorsMovie = null;
      })
      .addCase(fetchMovieByName.fulfilled, (state, action) => {
        console.log(action);
        state.movies = action.payload.results;
        state.totalPages = action.payload.total_pages;
        state.loadedMovie = true;
        state.loadingMovie = false;
      })
      .addCase(fetchMovieByName.rejected, (state, action) => {
        state.errorsMovie = action.error;
        state.loadedMovie = true;
        state.loadingMovie = false;
      });
  },
});

export const { addMovies, handleLikeButton, putInFavorites, changePage } = moviesSlice.actions;

export default moviesSlice.reducer;
