import { React, Suspense, lazy } from "react";
import { Navigate } from "react-router-dom";
import Login from "../pages/Login/Login";

const Dashboard = lazy(() => import("../pages/Dashboard"));
const MovieInfo = lazy(() => import("../pages/MovieInfo"));
const Favorites = lazy(() => import("../pages/Favorites"));

function getComponent(Component) {
  const TOKEN = localStorage.getItem("AUTH_TOKEN");
  console.log(TOKEN);
  return TOKEN ? (
    <Suspense>
      <Component />
    </Suspense>
  ) : (
    <Navigate to="/login" />
  );
}

export const routes = [
  {
    path: "/",
    element: getComponent(Dashboard),
  },
  {
    path: "/movie-info/:id",
    element: getComponent(MovieInfo),
  },
  {
    path: "/favorites",
    element: getComponent(Favorites),
  },
  {
    path: "login",
    element: (
      <Suspense>
        <Login />
      </Suspense>
    ),
  },
];
