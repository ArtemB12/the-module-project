import { useRoutes } from 'react-router-dom';
import { routes } from './routes';
import { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchMovies } from '../store/reducers/moviesSlice';


function RoutesList() {
  const { page } = useSelector((state) => state.movies);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovies({ page }));
  }, [dispatch, page]);

  let element = useRoutes(routes);
  return element;
}

export default RoutesList;
